/****************************************************************************
* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this file,
* You can obtain one at http://mozilla.org/MPL/2.0/.
*
* Copyright (c) 2016, shannon.mackey@refaqtory.com
* ***************************************************************************/
#ifndef VEQTOR_H
#define VEQTOR_H

#include "libveqtor_global.h"

#include <QDebug>
#include <QFile>
#include <QObject>
#include <QSortFilterProxyModel>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlQueryModel>

const QString SQLSETUP("://veqtor.sql");

class LIBVEQTORSHARED_EXPORT veQtor : public QObject {
    Q_OBJECT

    QSqlDatabase sqlite;
    QSqlQueryModel* m_QueryModel;
    QSortFilterProxyModel* m_ProxyModel;

    bool openDB(QString filename);
    bool createTables();

    QSqlQueryModel* QueryModel() const
    {
        return m_QueryModel;
    }

    void execSqlScript(QString sql_filename);
public:
    veQtor(QString filename, QObject* parent = 0);
    virtual ~veQtor() {}

    QSortFilterProxyModel* ProxyModel() const
    {
        return m_ProxyModel;
    }

    quint32 rowCount(QString select = "SELECT count(*) FROM veqtor");

public slots:
    void insert(QString node, QString property, QVariant content, qint32 stamp = 0);
};

#endif // VEQTOR_H
