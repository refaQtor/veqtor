/****************************************************************************
* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this file,
* You can obtain one at http://mozilla.org/MPL/2.0/.
*
* Copyright (c) 2016, shannon.mackey@refaqtory.com
* ***************************************************************************/
#ifndef LIBVEQTOR_GLOBAL_H
#define LIBVEQTOR_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(LIBVEQTOR_LIBRARY)
#  define LIBVEQTORSHARED_EXPORT Q_DECL_EXPORT
#else
#  define LIBVEQTORSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // LIBVEQTOR_GLOBAL_H
