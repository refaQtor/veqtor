/****************************************************************************
* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this file,
* You can obtain one at http://mozilla.org/MPL/2.0/.
*
* Copyright (c) 2016, shannon.mackey@refaqtory.com
* ***************************************************************************/
#include "veqtor.h"

#include <QFileInfo>
#include <QSqlError>
#include <QSqlResult>

veQtor::veQtor(QString filename, QObject* parent)
    : QObject(parent)
{
    if (openDB(filename)) {
        m_QueryModel = new QSqlQueryModel();
        m_QueryModel->setQuery("SELECT * FROM veqtor");

        m_ProxyModel = new QSortFilterProxyModel(this);
        m_ProxyModel->setSourceModel(m_QueryModel);
        m_ProxyModel->setFilterKeyColumn(-1);
    }
}

quint32 veQtor::rowCount(QString select)
{
    QSqlQuery count(sqlite);
    count.exec(select);
    count.first();
    return count.value(0).toInt();
}

void veQtor::insert(QString node, QString property, QVariant content, qint32 stamp)
{
    Q_ASSERT(!node.isEmpty());
    Q_ASSERT(!property.isEmpty());
    Q_ASSERT(!content.isNull());
    Q_ASSERT(stamp >= 0); //set to 0 by default in definition, therefore 0 = unset
    QSqlQuery ins;
    if (stamp == 0) {
        ins.prepare("INSERT INTO veqtor (node, property, content)"
                    "VALUES (:node, :property, :content)");
        ins.bindValue(":node", node);
        ins.bindValue(":property", property);
        ins.bindValue(":content", content);
    } else {
        ins.prepare("INSERT INTO veqtor (node, property, content, stamp)"
                    "VALUES (:node, :property, :content, :stamp )");
        ins.bindValue(":node", node);
        ins.bindValue(":property", property);
        ins.bindValue(":content", content);
        ins.bindValue(":stamp", stamp);
    }
    bool ok = ins.exec();
    if (!ok)
        qDebug() << ins.lastError();
    Q_ASSERT(ok);
}

bool veQtor::openDB(QString filename)
{
    QFileInfo dbf(filename);
    sqlite = QSqlDatabase::addDatabase("QSQLITE");
    sqlite.setDatabaseName(dbf.absoluteFilePath());
    if (!dbf.exists()) { //if it doesn't already exist, create one
        if (sqlite.open()) {
            qDebug() << "opened filename : " << filename;
            if (!createTables()) {
                qDebug("Database failed to initialize.");
                return false;
            }
        } else {
            qDebug("Database failed to open.");
            return false;
        }
    }
    qDebug() << "SQLite file: " << dbf.absoluteFilePath();
    return sqlite.open();
}

bool veQtor::createTables()
{
    bool ok = false;
    QFile script(SQLSETUP);
    if (script.open(QFile::ReadOnly | QFile::Text)) {
        QString scriptstring(script.readAll());
        QStringList queries = scriptstring.split("---");
        QSqlQuery query(sqlite);
        foreach (QString queryString, queries) {
            if (!queryString.simplified().isEmpty()) {
                ok = query.exec(queryString);
                if (!ok) {
                    QString error_string = QString("ERROR: query failed - %1")
                                               .arg(query.lastError().text());
                    qDebug() << error_string << queryString;
                }
            }
        }
        script.close();
        Q_ASSERT(ok);
        return true;
    } else {
        QString error_string = QString("ERROR: %1 failed to open - %2")
                                   .arg(script.fileName())
                                   .arg(script.errorString());
        qDebug() << error_string;
        Q_ASSERT(ok);
        return false;
    }
}
