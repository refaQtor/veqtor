CREATE TABLE veqtor
    (stamp INTEGER DEFAULT (strftime('%s','now')),
    edge TEXT UNIQUE DEFAULT (hex(randomblob(8))),
    node TEXT NOT NULL,
    property TEXT NOT NULL,
    content TEXT NOT NULL,
    inclusion REAL NOT NULL DEFAULT 1,
    PRIMARY KEY (stamp, edge));
---
CREATE INDEX stamp_index ON veqtor(stamp);
---
CREATE INDEX node_index ON veqtor(node);
---
CREATE INDEX property_index ON veqtor(property);
---
CREATE VIEW row_count AS
   SELECT count(*)
      FROM veqtor;
---
CREATE VIEW nodes AS
    SELECT DISTINCT node
      FROM veqtor;
---
CREATE VIEW property_content
    AS SELECT datetime(stamp, 'unixepoch') AS DateTime,
       property AS Type,
       content AS Value
  FROM veqtor;
---
CREATE TRIGGER property_content_insert
    INSTEAD OF INSERT
    ON property_content
    FOR EACH ROW
    BEGIN
        INSERT INTO veqtor (
            node,
            property,
            content)
        VALUES (
            hex(randomblob(8) ),
            new.Type,
            new.Value);
    END;
---
INSERT INTO veqtor (node, property, content) VALUES ('root', 'root', 'root');
