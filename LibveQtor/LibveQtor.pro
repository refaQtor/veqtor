#-------------------------------------------------
#
# Project created by QtCreator 2016-10-17T22:03:53
#
#-------------------------------------------------

QT       += sql

QT       -= gui

TARGET = libveQtor
TEMPLATE = lib

DEFINES += LIBVEQTOR_LIBRARY

SOURCES += veqtor.cpp

HEADERS += veqtor.h\
        libveqtor_global.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

DISTFILES +=

RESOURCES += \
    resources.qrc
