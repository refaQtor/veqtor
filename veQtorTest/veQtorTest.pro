#-------------------------------------------------
#
# Project created by QtCreator 2016-10-17T22:05:32
#
#-------------------------------------------------

QT       += testlib
QT       += sql

QT       -= gui

TARGET = tst_veqtortesttest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += tst_veqtortesttest.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../LibveQtor/release/ -llibveQtor
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../LibveQtor/debug/ -llibveQtor
else:unix: LIBS += -L$$OUT_PWD/../LibveQtor/ -llibveQtor

INCLUDEPATH += $$PWD/../LibveQtor
DEPENDPATH += $$PWD/../LibveQtor
