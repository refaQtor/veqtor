/****************************************************************************
* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this file,
* You can obtain one at http://mozilla.org/MPL/2.0/.
*
* Copyright (c) 2016, shannon.mackey@refaqtory.com
* ***************************************************************************/
#include <QFile>
#include <QString>
#include <QtTest>

#include "veqtor.h"

static const QString TESTFILE("veqtorTest");

class VeQtorTestTest : public QObject {
    Q_OBJECT

    veQtor* v;

public:
    VeQtorTestTest();

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();

    void insertDefault();
    void insertStamp();
};

VeQtorTestTest::VeQtorTestTest()
{
}

void VeQtorTestTest::initTestCase()
{
    QFile::remove(TESTFILE);
    v = new veQtor(TESTFILE);
}

void VeQtorTestTest::cleanupTestCase()
{
}

void VeQtorTestTest::insertDefault()
{
    QString countString("SELECT count(*) FROM veqtor WHERE property = 'tag1';");
    quint32 beginRowcount = v->rowCount(countString);
    v->insert("node1", "tag1", "value1");
    quint32 rows = v->rowCount(countString);
    QVERIFY(rows == beginRowcount + 1);
}

void VeQtorTestTest::insertStamp()
{
    QString countString("SELECT count(*) FROM veqtor WHERE node = 'node2';");
    quint32 beginRowcount = v->rowCount(countString);
    v->insert("node2", "tag2", "value2", 1234567890);
    v->insert("node2", "tag3", "value3", 1234567891);
    quint32 rows = v->rowCount(countString);
    QVERIFY(rows == beginRowcount + 2);
}

QTEST_APPLESS_MAIN(VeQtorTestTest)

#include "tst_veqtortesttest.moc"
